<?php
/**
 * StorePickupTimeForReplaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. BU. Продавцы
 *
 * Управление продавцами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\BuClient;

use PHPUnit\Framework\TestCase;

/**
 * StorePickupTimeForReplaceTest Class Doc Comment
 *
 * @category    Class
 * @description StorePickupTimeForReplace
 * @package     Ensi\BuClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class StorePickupTimeForReplaceTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "StorePickupTimeForReplace"
     */
    public function testStorePickupTimeForReplace()
    {
    }

    /**
     * Test attribute "store_id"
     */
    public function testPropertyStoreId()
    {
    }

    /**
     * Test attribute "day"
     */
    public function testPropertyDay()
    {
    }

    /**
     * Test attribute "pickup_time_code"
     */
    public function testPropertyPickupTimeCode()
    {
    }

    /**
     * Test attribute "pickup_time_start"
     */
    public function testPropertyPickupTimeStart()
    {
    }

    /**
     * Test attribute "pickup_time_end"
     */
    public function testPropertyPickupTimeEnd()
    {
    }

    /**
     * Test attribute "cargo_export_time"
     */
    public function testPropertyCargoExportTime()
    {
    }

    /**
     * Test attribute "delivery_service"
     */
    public function testPropertyDeliveryService()
    {
    }
}
