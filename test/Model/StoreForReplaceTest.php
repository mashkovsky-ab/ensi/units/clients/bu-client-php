<?php
/**
 * StoreForReplaceTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. BU. Продавцы
 *
 * Управление продавцами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\BuClient;

use PHPUnit\Framework\TestCase;

/**
 * StoreForReplaceTest Class Doc Comment
 *
 * @category    Class
 * @description StoreForReplace
 * @package     Ensi\BuClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class StoreForReplaceTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "StoreForReplace"
     */
    public function testStoreForReplace()
    {
    }

    /**
     * Test attribute "seller_id"
     */
    public function testPropertySellerId()
    {
    }

    /**
     * Test attribute "xml_id"
     */
    public function testPropertyXmlId()
    {
    }

    /**
     * Test attribute "active"
     */
    public function testPropertyActive()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "address"
     */
    public function testPropertyAddress()
    {
    }

    /**
     * Test attribute "timezone"
     */
    public function testPropertyTimezone()
    {
    }
}
