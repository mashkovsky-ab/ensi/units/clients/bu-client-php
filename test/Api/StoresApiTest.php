<?php
/**
 * StoresApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. BU. Продавцы
 *
 * Управление продавцами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\BuClient;

use \Ensi\BuClient\Configuration;
use \Ensi\BuClient\ApiException;
use \Ensi\BuClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * StoresApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class StoresApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createStore
     *
     * Добавить новый склад.
     *
     */
    public function testCreateStore()
    {
    }

    /**
     * Test case for deleteStore
     *
     * Удаление склада.
     *
     */
    public function testDeleteStore()
    {
    }

    /**
     * Test case for getStore
     *
     * Получить информацию о складе.
     *
     */
    public function testGetStore()
    {
    }

    /**
     * Test case for patchStore
     *
     * Частично обновить информацию о складе.
     *
     */
    public function testPatchStore()
    {
    }

    /**
     * Test case for replaceStore
     *
     * Полностью обновить информацию о складе.
     *
     */
    public function testReplaceStore()
    {
    }

    /**
     * Test case for searchOneStore
     *
     * Поиск склада, удовлетворяющего условиям.
     *
     */
    public function testSearchOneStore()
    {
    }

    /**
     * Test case for searchStores
     *
     * Получить список складов, удовлетворяющих условиям.
     *
     */
    public function testSearchStores()
    {
    }
}
