<?php
/**
 * SellersApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. BU. Продавцы
 *
 * Управление продавцами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\BuClient;

use \Ensi\BuClient\Configuration;
use \Ensi\BuClient\ApiException;
use \Ensi\BuClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SellersApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SellersApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createSeller
     *
     * Добавить нового продавца.
     *
     */
    public function testCreateSeller()
    {
    }

    /**
     * Test case for createSellerWithOperator
     *
     * Добавить нового продавца с оператором.
     *
     */
    public function testCreateSellerWithOperator()
    {
    }

    /**
     * Test case for deleteSeller
     *
     * Удаление продавца.
     *
     */
    public function testDeleteSeller()
    {
    }

    /**
     * Test case for getSeller
     *
     * Получить информацию о продавце.
     *
     */
    public function testGetSeller()
    {
    }

    /**
     * Test case for patchSeller
     *
     * Частично обновить информацию о продавце.
     *
     */
    public function testPatchSeller()
    {
    }

    /**
     * Test case for registerSellerWithOperator
     *
     * Зарегистрировать нового продавца.
     *
     */
    public function testRegisterSellerWithOperator()
    {
    }

    /**
     * Test case for replaceSeller
     *
     * Полностью обновить информацию о продавце.
     *
     */
    public function testReplaceSeller()
    {
    }

    /**
     * Test case for searchSellers
     *
     * Получить список продавецов, удовлетворяющих условиям.
     *
     */
    public function testSearchSellers()
    {
    }
}
