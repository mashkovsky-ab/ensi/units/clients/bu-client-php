<?php
/**
 * StoreIncludes
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi. BU. Продавцы
 *
 * Управление продавцами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\BuClient\Dto;

use \ArrayAccess;
use \Ensi\BuClient\ObjectSerializer;

/**
 * StoreIncludes Class Doc Comment
 *
 * @category Class
 * @package  Ensi\BuClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class StoreIncludes implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'StoreIncludes';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'workings' => '\Ensi\BuClient\Dto\StoreWorking[]',
        'contacts' => '\Ensi\BuClient\Dto\StoreContact[]',
        'contact' => '\Ensi\BuClient\Dto\StoreContact',
        'pickup_times' => '\Ensi\BuClient\Dto\StorePickupTime[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'workings' => null,
        'contacts' => null,
        'contact' => null,
        'pickup_times' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'workings' => false,
        'contacts' => false,
        'contact' => false,
        'pickup_times' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::$openAPINullables[$property])) {
            return self::$openAPINullables[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'workings' => 'workings',
        'contacts' => 'contacts',
        'contact' => 'contact',
        'pickup_times' => 'pickup_times'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'workings' => 'setWorkings',
        'contacts' => 'setContacts',
        'contact' => 'setContact',
        'pickup_times' => 'setPickupTimes'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'workings' => 'getWorkings',
        'contacts' => 'getContacts',
        'contact' => 'getContact',
        'pickup_times' => 'getPickupTimes'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('workings', $data, null);
        $this->setIfExists('contacts', $data, null);
        $this->setIfExists('contact', $data, null);
        $this->setIfExists('pickup_times', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets workings
     *
     * @return \Ensi\BuClient\Dto\StoreWorking[]|null
     */
    public function getWorkings()
    {
        return $this->container['workings'];
    }

    /**
     * Sets workings
     *
     * @param \Ensi\BuClient\Dto\StoreWorking[]|null $workings workings
     *
     * @return $this
     */
    public function setWorkings($workings)
    {


        /*if (is_null($workings)) {
            throw new \InvalidArgumentException('non-nullable workings cannot be null');
        }*/
        $this->container['workings'] = $workings;

        return $this;
    }

    /**
     * Gets contacts
     *
     * @return \Ensi\BuClient\Dto\StoreContact[]|null
     */
    public function getContacts()
    {
        return $this->container['contacts'];
    }

    /**
     * Sets contacts
     *
     * @param \Ensi\BuClient\Dto\StoreContact[]|null $contacts contacts
     *
     * @return $this
     */
    public function setContacts($contacts)
    {


        /*if (is_null($contacts)) {
            throw new \InvalidArgumentException('non-nullable contacts cannot be null');
        }*/
        $this->container['contacts'] = $contacts;

        return $this;
    }

    /**
     * Gets contact
     *
     * @return \Ensi\BuClient\Dto\StoreContact|null
     */
    public function getContact()
    {
        return $this->container['contact'];
    }

    /**
     * Sets contact
     *
     * @param \Ensi\BuClient\Dto\StoreContact|null $contact contact
     *
     * @return $this
     */
    public function setContact($contact)
    {


        /*if (is_null($contact)) {
            throw new \InvalidArgumentException('non-nullable contact cannot be null');
        }*/
        $this->container['contact'] = $contact;

        return $this;
    }

    /**
     * Gets pickup_times
     *
     * @return \Ensi\BuClient\Dto\StorePickupTime[]|null
     */
    public function getPickupTimes()
    {
        return $this->container['pickup_times'];
    }

    /**
     * Sets pickup_times
     *
     * @param \Ensi\BuClient\Dto\StorePickupTime[]|null $pickup_times pickup_times
     *
     * @return $this
     */
    public function setPickupTimes($pickup_times)
    {


        /*if (is_null($pickup_times)) {
            throw new \InvalidArgumentException('non-nullable pickup_times cannot be null');
        }*/
        $this->container['pickup_times'] = $pickup_times;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


