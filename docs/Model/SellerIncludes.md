# # SellerIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operators** | [**\Ensi\BuClient\Dto\Operator[]**](Operator.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


