# # OperatorForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_id** | **int** | ID продавца | [optional] 
**user_id** | **int** | ID пользователя | [optional] 
**is_receive_sms** | **bool** | Получатель смс | [optional] 
**is_main** | **bool** | Главное контактное лицо продавца | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


