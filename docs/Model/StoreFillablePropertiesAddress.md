# # StoreFillablePropertiesAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_string** | **string** | Адрес одной строкой | [optional] 
**country_code** | **string** | Код страны | [optional] 
**post_index** | **string** | Почтовый индекс | [optional] 
**region** | **string** | Регион | [optional] 
**region_guid** | **string** | ФИАС ID региона | [optional] 
**area** | **string** | Район в регионе | [optional] 
**area_guid** | **string** | ФИАС ID района в регионе | [optional] 
**city** | **string** | Город/населенный пункт | [optional] 
**city_guid** | **string** | ФИАС ID города/населенного пункта | [optional] 
**street** | **string** | Улица | [optional] 
**house** | **string** | Дом | [optional] 
**block** | **string** | Строение/корпус | [optional] 
**flat** | **string** | Квартира/офис | [optional] 
**porch** | **string** | Подъезд | [optional] 
**floor** | **string** | Этаж | [optional] 
**intercom** | **string** | Домофон | [optional] 
**comment** | **string** | Комментарий к адресу | [optional] 
**geo_lat** | **string** | координаты: широта | [optional] 
**geo_lon** | **string** | координаты: долгота | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


