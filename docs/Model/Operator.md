# # Operator

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор оператора | [optional] 
**created_at** | **string** | Время создания оператора | [optional] 
**updated_at** | **string** | Время обновления оператора | [optional] 
**seller_id** | **int** | ID продавца | [optional] 
**user_id** | **int** | ID пользователя | [optional] 
**is_receive_sms** | **bool** | Получатель смс | [optional] 
**is_main** | **bool** | Главное контактное лицо продавца | [optional] 
**seller** | [**\Ensi\BuClient\Dto\Seller**](Seller.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


