# # CreateWithOperatorOrRegisterSellerRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller** | [**\Ensi\BuClient\Dto\SellerForCreate**](SellerForCreate.md) |  | [optional] 
**operator** | [**\Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequestOperator**](CreateWithOperatorOrRegisterSellerRequestOperator.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


