# # StoreContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор контактного лица для склада | [optional] 
**created_at** | **string** | Время создания контактного лица для склада | [optional] 
**updated_at** | **string** | Время обновления контактного лица для склада | [optional] 
**store_id** | **int** | ID склада | [optional] 
**name** | **string** | Имя контактного лица | [optional] 
**phone** | **string** | Телефон контактного лица | [optional] 
**email** | **string** | Email контактного лица | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


