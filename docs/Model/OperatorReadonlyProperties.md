# # OperatorReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор оператора | [optional] 
**created_at** | **string** | Время создания оператора | [optional] 
**updated_at** | **string** | Время обновления оператора | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


