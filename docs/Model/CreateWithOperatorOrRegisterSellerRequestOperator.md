# # CreateWithOperatorOrRegisterSellerRequestOperator

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** | Имя оператора | [optional] 
**middle_name** | **string** | Отчество оператора | [optional] 
**last_name** | **string** | Фамилия оператора | [optional] 
**email** | **string** | Email | [optional] 
**phone** | **string** | Телефон | [optional] 
**password** | **string** | Пароль | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


