# # SellerFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_name** | **string** | Юридическое наименование организации | [optional] 
**external_id** | **string** | Внешний код | [optional] 
**inn** | **string** | ИНН | [optional] 
**kpp** | **string** | КПП | [optional] 
**legal_address** | **string** | Юр. адрес | [optional] 
**fact_address** | **string** | Фактический адрес | [optional] 
**payment_account** | **string** | Номер банковского счета | [optional] 
**bank** | **string** | Наименование банка | [optional] 
**bank_address** | **string** | Юридический адрес банка | [optional] 
**bank_bik** | **string** | БИК банка | [optional] 
**status** | [**\Ensi\BuClient\Dto\SellerStatusEnum**](SellerStatusEnum.md) |  | [optional] 
**manager_id** | **int** | ID менеджера | [optional] 
**correspondent_account** | **string** | Номер корреспондентского счета банка | [optional] 
**storage_address** | **string** | Адреса складов отгрузки | [optional] 
**site** | **string** | Сайт компании | [optional] 
**can_integration** | **bool** | Подтверждение о возможности работы с Платформой с использованием автоматических механизмов интеграции | [optional] 
**sale_info** | **string** | Бренды и товары, которыми торгует Продавец | [optional] 
**city** | **string** | Город продавца | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


