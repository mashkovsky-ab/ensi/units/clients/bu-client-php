# # StoreForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_id** | **int** | ID продавца | [optional] 
**xml_id** | **string** | ID склада у продавца | [optional] 
**active** | **bool** | Флаг активности склада | [optional] 
**name** | **string** | Название | [optional] 
**address** | [**\Ensi\BuClient\Dto\StoreFillablePropertiesAddress**](StoreFillablePropertiesAddress.md) |  | [optional] 
**timezone** | **string** | Часовой пояс | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


