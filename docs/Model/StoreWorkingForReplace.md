# # StoreWorkingForReplace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_id** | **int** | ID склада | [optional] 
**active** | **bool** | Флаг активности дня работы склада | [optional] 
**day** | **int** | День недели (1-7) | [optional] 
**working_start_time** | **string** | Время начала работы склада (00:00) | [optional] 
**working_end_time** | **string** | Время конца работы склада (00:00) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


