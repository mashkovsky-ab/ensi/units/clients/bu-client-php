# # StoreWorkingReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор записи о времени работы | [optional] 
**created_at** | **string** | Время создания записи о времени работы | [optional] 
**updated_at** | **string** | Время обновления записи о времени работы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


