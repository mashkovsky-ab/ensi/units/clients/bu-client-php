# # SellerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор продавца | [optional] 
**status_at** | **string** | Время изменения статуса продавца | [optional] 
**created_at** | **string** | Время создания продавца | [optional] 
**updated_at** | **string** | Время обновления продавца | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


