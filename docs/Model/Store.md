# # Store

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор склада | [optional] 
**created_at** | **string** | Время создания склада | [optional] 
**updated_at** | **string** | Время обновления склада | [optional] 
**seller_id** | **int** | ID продавца | [optional] 
**xml_id** | **string** | ID склада у продавца | [optional] 
**active** | **bool** | Флаг активности склада | [optional] 
**name** | **string** | Название | [optional] 
**address** | [**\Ensi\BuClient\Dto\StoreFillablePropertiesAddress**](StoreFillablePropertiesAddress.md) |  | [optional] 
**timezone** | **string** | Часовой пояс | [optional] 
**workings** | [**\Ensi\BuClient\Dto\StoreWorking[]**](StoreWorking.md) |  | [optional] 
**contacts** | [**\Ensi\BuClient\Dto\StoreContact[]**](StoreContact.md) |  | [optional] 
**contact** | [**\Ensi\BuClient\Dto\StoreContact**](StoreContact.md) |  | [optional] 
**pickup_times** | [**\Ensi\BuClient\Dto\StorePickupTime[]**](StorePickupTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


