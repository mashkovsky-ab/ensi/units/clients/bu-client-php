# Ensi\BuClient\StoreWorkingsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStoreWorking**](StoreWorkingsApi.md#createStoreWorking) | **POST** /stores/workings | Добавить новую запись о времени работы склада
[**deleteStoreWorking**](StoreWorkingsApi.md#deleteStoreWorking) | **DELETE** /stores/workings/{id} | Удалить запись о времени работы склада
[**getStoreWorking**](StoreWorkingsApi.md#getStoreWorking) | **GET** /stores/workings/{id} | Получить запись о времени работы склада
[**patchStoreWorking**](StoreWorkingsApi.md#patchStoreWorking) | **PATCH** /stores/workings/{id} | Частично обновить запись о времени работы склада
[**replaceStoreWorking**](StoreWorkingsApi.md#replaceStoreWorking) | **PUT** /stores/workings/{id} | Полностью обновить запись о времени работы склада
[**searchStoreWorkings**](StoreWorkingsApi.md#searchStoreWorkings) | **POST** /stores/workings:search | Получить список записей о времени работы склада



## createStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse createStoreWorking($store_working_for_create)

Добавить новую запись о времени работы склада

Создание объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$store_working_for_create = new \Ensi\BuClient\Dto\StoreWorkingForCreate(); // \Ensi\BuClient\Dto\StoreWorkingForCreate | 

try {
    $result = $apiInstance->createStoreWorking($store_working_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->createStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_working_for_create** | [**\Ensi\BuClient\Dto\StoreWorkingForCreate**](../Model/StoreWorkingForCreate.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStoreWorking

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStoreWorking($id)

Удалить запись о времени работы склада

Удаление объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStoreWorking($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->deleteStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse getStoreWorking($id, $include)

Получить запись о времени работы склада

Получение объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStoreWorking($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->getStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse patchStoreWorking($id, $store_working_for_patch)

Частично обновить запись о времени работы склада

Обновления части полей объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_working_for_patch = new \Ensi\BuClient\Dto\StoreWorkingForPatch(); // \Ensi\BuClient\Dto\StoreWorkingForPatch | 

try {
    $result = $apiInstance->patchStoreWorking($id, $store_working_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->patchStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_working_for_patch** | [**\Ensi\BuClient\Dto\StoreWorkingForPatch**](../Model/StoreWorkingForPatch.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse replaceStoreWorking($id, $store_working_for_replace)

Полностью обновить запись о времени работы склада

Замена объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_working_for_replace = new \Ensi\BuClient\Dto\StoreWorkingForReplace(); // \Ensi\BuClient\Dto\StoreWorkingForReplace | 

try {
    $result = $apiInstance->replaceStoreWorking($id, $store_working_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->replaceStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_working_for_replace** | [**\Ensi\BuClient\Dto\StoreWorkingForReplace**](../Model/StoreWorkingForReplace.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStoreWorkings

> \Ensi\BuClient\Dto\SearchStoreWorkingsResponse searchStoreWorkings($search_store_workings_request)

Получить список записей о времени работы склада

Поиск объектов типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_workings_request = new \Ensi\BuClient\Dto\SearchStoreWorkingsRequest(); // \Ensi\BuClient\Dto\SearchStoreWorkingsRequest | 

try {
    $result = $apiInstance->searchStoreWorkings($search_store_workings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->searchStoreWorkings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_workings_request** | [**\Ensi\BuClient\Dto\SearchStoreWorkingsRequest**](../Model/SearchStoreWorkingsRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoreWorkingsResponse**](../Model/SearchStoreWorkingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

