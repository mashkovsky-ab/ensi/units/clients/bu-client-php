# Ensi\BuClient\StoresApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStore**](StoresApi.md#createStore) | **POST** /stores/stores | Добавить новый склад
[**deleteStore**](StoresApi.md#deleteStore) | **DELETE** /stores/stores/{id} | Удаление склада
[**getStore**](StoresApi.md#getStore) | **GET** /stores/stores/{id} | Получить информацию о складе
[**patchStore**](StoresApi.md#patchStore) | **PATCH** /stores/stores/{id} | Частично обновить информацию о складе
[**replaceStore**](StoresApi.md#replaceStore) | **PUT** /stores/stores/{id} | Полностью обновить информацию о складе
[**searchOneStore**](StoresApi.md#searchOneStore) | **POST** /stores/stores:search-one | Поиск склада, удовлетворяющего условиям
[**searchStores**](StoresApi.md#searchStores) | **POST** /stores/stores:search | Получить список складов, удовлетворяющих условиям



## createStore

> \Ensi\BuClient\Dto\StoreResponse createStore($store_for_create)

Добавить новый склад

Создание объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$store_for_create = new \Ensi\BuClient\Dto\StoreForCreate(); // \Ensi\BuClient\Dto\StoreForCreate | 

try {
    $result = $apiInstance->createStore($store_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->createStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_for_create** | [**\Ensi\BuClient\Dto\StoreForCreate**](../Model/StoreForCreate.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStore

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStore($id)

Удаление склада

Удаление объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStore($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->deleteStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStore

> \Ensi\BuClient\Dto\StoreResponse getStore($id, $include)

Получить информацию о складе

Получение объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStore($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->getStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStore

> \Ensi\BuClient\Dto\StoreResponse patchStore($id, $store_for_patch)

Частично обновить информацию о складе

Обновления части полей объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_for_patch = new \Ensi\BuClient\Dto\StoreForPatch(); // \Ensi\BuClient\Dto\StoreForPatch | 

try {
    $result = $apiInstance->patchStore($id, $store_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->patchStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_for_patch** | [**\Ensi\BuClient\Dto\StoreForPatch**](../Model/StoreForPatch.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceStore

> \Ensi\BuClient\Dto\StoreResponse replaceStore($id, $store_for_replace)

Полностью обновить информацию о складе

Замена объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_for_replace = new \Ensi\BuClient\Dto\StoreForReplace(); // \Ensi\BuClient\Dto\StoreForReplace | 

try {
    $result = $apiInstance->replaceStore($id, $store_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->replaceStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_for_replace** | [**\Ensi\BuClient\Dto\StoreForReplace**](../Model/StoreForReplace.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneStore

> OneOfStoreResponseEmptyDataResponse searchOneStore($search_stores_request)

Поиск склада, удовлетворяющего условиям

Поиск объектов типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stores_request = new \Ensi\BuClient\Dto\SearchStoresRequest(); // \Ensi\BuClient\Dto\SearchStoresRequest | 

try {
    $result = $apiInstance->searchOneStore($search_stores_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->searchOneStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stores_request** | [**\Ensi\BuClient\Dto\SearchStoresRequest**](../Model/SearchStoresRequest.md)|  |

### Return type

[**OneOfStoreResponseEmptyDataResponse**](../Model/OneOfStoreResponseEmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStores

> \Ensi\BuClient\Dto\SearchStoresResponse searchStores($search_stores_request)

Получить список складов, удовлетворяющих условиям

Поиск объектов типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stores_request = new \Ensi\BuClient\Dto\SearchStoresRequest(); // \Ensi\BuClient\Dto\SearchStoresRequest | 

try {
    $result = $apiInstance->searchStores($search_stores_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->searchStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stores_request** | [**\Ensi\BuClient\Dto\SearchStoresRequest**](../Model/SearchStoresRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoresResponse**](../Model/SearchStoresResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

