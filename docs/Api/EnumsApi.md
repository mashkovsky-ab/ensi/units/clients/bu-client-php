# Ensi\BuClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSellerStatuses**](EnumsApi.md#getSellerStatuses) | **GET** /sellers/seller-statuses | Получение объектов типа SellerStatus



## getSellerStatuses

> \Ensi\BuClient\Dto\SellerStatusesResponse getSellerStatuses()

Получение объектов типа SellerStatus

Получение объектов типа SellerStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getSellerStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getSellerStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\BuClient\Dto\SellerStatusesResponse**](../Model/SellerStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

