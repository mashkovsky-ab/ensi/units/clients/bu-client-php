# Ensi\BuClient\SellersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSeller**](SellersApi.md#createSeller) | **POST** /sellers/sellers | Добавить нового продавца
[**createSellerWithOperator**](SellersApi.md#createSellerWithOperator) | **POST** /sellers/sellers:create | Добавить нового продавца с оператором
[**deleteSeller**](SellersApi.md#deleteSeller) | **DELETE** /sellers/sellers/{id} | Удаление продавца
[**getSeller**](SellersApi.md#getSeller) | **GET** /sellers/sellers/{id} | Получить информацию о продавце
[**patchSeller**](SellersApi.md#patchSeller) | **PATCH** /sellers/sellers/{id} | Частично обновить информацию о продавце
[**registerSellerWithOperator**](SellersApi.md#registerSellerWithOperator) | **POST** /sellers/sellers:register | Зарегистрировать нового продавца
[**replaceSeller**](SellersApi.md#replaceSeller) | **PUT** /sellers/sellers/{id} | Полностью обновить информацию о продавце
[**searchSellers**](SellersApi.md#searchSellers) | **POST** /sellers/sellers:search | Получить список продавецов, удовлетворяющих условиям



## createSeller

> \Ensi\BuClient\Dto\SellerResponse createSeller($seller_for_create)

Добавить нового продавца

Создание объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_for_create = new \Ensi\BuClient\Dto\SellerForCreate(); // \Ensi\BuClient\Dto\SellerForCreate | 

try {
    $result = $apiInstance->createSeller($seller_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->createSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_for_create** | [**\Ensi\BuClient\Dto\SellerForCreate**](../Model/SellerForCreate.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createSellerWithOperator

> \Ensi\BuClient\Dto\SellerResponse createSellerWithOperator($create_with_operator_or_register_seller_request)

Добавить нового продавца с оператором

Создание объекта типа Seller со статусом \"Одобрен\", объекта Operator (также под капотом создается объект User в мс Admin-Auth)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_with_operator_or_register_seller_request = new \Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest(); // \Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest | 

try {
    $result = $apiInstance->createSellerWithOperator($create_with_operator_or_register_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->createSellerWithOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_with_operator_or_register_seller_request** | [**\Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest**](../Model/CreateWithOperatorOrRegisterSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteSeller

> \Ensi\BuClient\Dto\EmptyDataResponse deleteSeller($id)

Удаление продавца

Удаление объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteSeller($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->deleteSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSeller

> \Ensi\BuClient\Dto\SellerResponse getSeller($id, $include)

Получить информацию о продавце

Получение объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getSeller($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->getSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSeller

> \Ensi\BuClient\Dto\SellerResponse patchSeller($id, $seller_for_patch)

Частично обновить информацию о продавце

Обновление части полей объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$seller_for_patch = new \Ensi\BuClient\Dto\SellerForPatch(); // \Ensi\BuClient\Dto\SellerForPatch | 

try {
    $result = $apiInstance->patchSeller($id, $seller_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->patchSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **seller_for_patch** | [**\Ensi\BuClient\Dto\SellerForPatch**](../Model/SellerForPatch.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## registerSellerWithOperator

> \Ensi\BuClient\Dto\SellerResponse registerSellerWithOperator($create_with_operator_or_register_seller_request)

Зарегистрировать нового продавца

Создание объекта типа Seller со статусом \"Только создан, информации нет\", объекта Operator (также под капотом создается объект User в мс Admin-Auth)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_with_operator_or_register_seller_request = new \Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest(); // \Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest | 

try {
    $result = $apiInstance->registerSellerWithOperator($create_with_operator_or_register_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->registerSellerWithOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_with_operator_or_register_seller_request** | [**\Ensi\BuClient\Dto\CreateWithOperatorOrRegisterSellerRequest**](../Model/CreateWithOperatorOrRegisterSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceSeller

> \Ensi\BuClient\Dto\SellerResponse replaceSeller($id, $seller_for_replace)

Полностью обновить информацию о продавце

Замена объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$seller_for_replace = new \Ensi\BuClient\Dto\SellerForReplace(); // \Ensi\BuClient\Dto\SellerForReplace | 

try {
    $result = $apiInstance->replaceSeller($id, $seller_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->replaceSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **seller_for_replace** | [**\Ensi\BuClient\Dto\SellerForReplace**](../Model/SellerForReplace.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSellers

> \Ensi\BuClient\Dto\SearchSellersResponse searchSellers($search_sellers_request)

Получить список продавецов, удовлетворяющих условиям

Поиск объектов типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_sellers_request = new \Ensi\BuClient\Dto\SearchSellersRequest(); // \Ensi\BuClient\Dto\SearchSellersRequest | 

try {
    $result = $apiInstance->searchSellers($search_sellers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->searchSellers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_sellers_request** | [**\Ensi\BuClient\Dto\SearchSellersRequest**](../Model/SearchSellersRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchSellersResponse**](../Model/SearchSellersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

