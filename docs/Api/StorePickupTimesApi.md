# Ensi\BuClient\StorePickupTimesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStorePickupTime**](StorePickupTimesApi.md#createStorePickupTime) | **POST** /stores/pickup-times | Добавить новую запись о времени отгрузки со склада
[**deleteStorePickupTime**](StorePickupTimesApi.md#deleteStorePickupTime) | **DELETE** /stores/pickup-times/{id} | Удалить запись о времени отгрузки со склада
[**getStorePickupTime**](StorePickupTimesApi.md#getStorePickupTime) | **GET** /stores/pickup-times/{id} | Получить запись о времени отгрузки со склада
[**patchStorePickupTime**](StorePickupTimesApi.md#patchStorePickupTime) | **PATCH** /stores/pickup-times/{id} | Частично обновить запись о времени отгрузки со склада
[**replaceStorePickupTime**](StorePickupTimesApi.md#replaceStorePickupTime) | **PUT** /stores/pickup-times/{id} | Полностью обновить запись о времени отгрузки со склада
[**searchStorePickupTimes**](StorePickupTimesApi.md#searchStorePickupTimes) | **POST** /stores/pickup-times:search | Получить список записей о времени отгрузки со склада



## createStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse createStorePickupTime($store_pickup_time_for_create)

Добавить новую запись о времени отгрузки со склада

Создание объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$store_pickup_time_for_create = new \Ensi\BuClient\Dto\StorePickupTimeForCreate(); // \Ensi\BuClient\Dto\StorePickupTimeForCreate | 

try {
    $result = $apiInstance->createStorePickupTime($store_pickup_time_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->createStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_pickup_time_for_create** | [**\Ensi\BuClient\Dto\StorePickupTimeForCreate**](../Model/StorePickupTimeForCreate.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStorePickupTime

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStorePickupTime($id)

Удалить запись о времени отгрузки со склада

Удаление объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStorePickupTime($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->deleteStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse getStorePickupTime($id, $include)

Получить запись о времени отгрузки со склада

Получение объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStorePickupTime($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->getStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse patchStorePickupTime($id, $store_pickup_time_for_patch)

Частично обновить запись о времени отгрузки со склада

Обновления части полей объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_pickup_time_for_patch = new \Ensi\BuClient\Dto\StorePickupTimeForPatch(); // \Ensi\BuClient\Dto\StorePickupTimeForPatch | 

try {
    $result = $apiInstance->patchStorePickupTime($id, $store_pickup_time_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->patchStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_pickup_time_for_patch** | [**\Ensi\BuClient\Dto\StorePickupTimeForPatch**](../Model/StorePickupTimeForPatch.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse replaceStorePickupTime($id, $store_pickup_time_for_replace)

Полностью обновить запись о времени отгрузки со склада

Замена объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_pickup_time_for_replace = new \Ensi\BuClient\Dto\StorePickupTimeForReplace(); // \Ensi\BuClient\Dto\StorePickupTimeForReplace | 

try {
    $result = $apiInstance->replaceStorePickupTime($id, $store_pickup_time_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->replaceStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_pickup_time_for_replace** | [**\Ensi\BuClient\Dto\StorePickupTimeForReplace**](../Model/StorePickupTimeForReplace.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStorePickupTimes

> \Ensi\BuClient\Dto\SearchStorePickupTimesResponse searchStorePickupTimes($search_store_pickup_times_request)

Получить список записей о времени отгрузки со склада

Поиск объектов типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_pickup_times_request = new \Ensi\BuClient\Dto\SearchStorePickupTimesRequest(); // \Ensi\BuClient\Dto\SearchStorePickupTimesRequest | 

try {
    $result = $apiInstance->searchStorePickupTimes($search_store_pickup_times_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->searchStorePickupTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_pickup_times_request** | [**\Ensi\BuClient\Dto\SearchStorePickupTimesRequest**](../Model/SearchStorePickupTimesRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStorePickupTimesResponse**](../Model/SearchStorePickupTimesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

