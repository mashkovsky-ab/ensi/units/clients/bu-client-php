# Ensi\BuClient\StoreContactsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStoreContact**](StoreContactsApi.md#createStoreContact) | **POST** /stores/contacts | Добавить новое контактное лицо склада
[**deleteStoreContact**](StoreContactsApi.md#deleteStoreContact) | **DELETE** /stores/contacts/{id} | Удалить информацию о контактном лице склада
[**getStoreContact**](StoreContactsApi.md#getStoreContact) | **GET** /stores/contacts/{id} | Получить информацию о контактном лице склада
[**patchStoreContact**](StoreContactsApi.md#patchStoreContact) | **PATCH** /stores/contacts/{id} | Частично обновить информацию о контактном лице  склада
[**replaceStoreContact**](StoreContactsApi.md#replaceStoreContact) | **PUT** /stores/contacts/{id} | Полностью обновить информацию о контактном лице  склада
[**searchStoreContacts**](StoreContactsApi.md#searchStoreContacts) | **POST** /stores/contacts:search | Получить список контактных лиц склада, удовлетворяющих условиям



## createStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse createStoreContact($store_contact_for_create)

Добавить новое контактное лицо склада

Создание объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$store_contact_for_create = new \Ensi\BuClient\Dto\StoreContactForCreate(); // \Ensi\BuClient\Dto\StoreContactForCreate | 

try {
    $result = $apiInstance->createStoreContact($store_contact_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->createStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_contact_for_create** | [**\Ensi\BuClient\Dto\StoreContactForCreate**](../Model/StoreContactForCreate.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStoreContact

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStoreContact($id)

Удалить информацию о контактном лице склада

Удаление объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStoreContact($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->deleteStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse getStoreContact($id, $include)

Получить информацию о контактном лице склада

Получение объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStoreContact($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->getStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse patchStoreContact($id, $store_contact_for_patch)

Частично обновить информацию о контактном лице  склада

Обновления части полей объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_contact_for_patch = new \Ensi\BuClient\Dto\StoreContactForPatch(); // \Ensi\BuClient\Dto\StoreContactForPatch | 

try {
    $result = $apiInstance->patchStoreContact($id, $store_contact_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->patchStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_contact_for_patch** | [**\Ensi\BuClient\Dto\StoreContactForPatch**](../Model/StoreContactForPatch.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse replaceStoreContact($id, $store_contact_for_replace)

Полностью обновить информацию о контактном лице  склада

Замена объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$store_contact_for_replace = new \Ensi\BuClient\Dto\StoreContactForReplace(); // \Ensi\BuClient\Dto\StoreContactForReplace | 

try {
    $result = $apiInstance->replaceStoreContact($id, $store_contact_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->replaceStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **store_contact_for_replace** | [**\Ensi\BuClient\Dto\StoreContactForReplace**](../Model/StoreContactForReplace.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStoreContacts

> \Ensi\BuClient\Dto\SearchStoreContactsResponse searchStoreContacts($search_store_contacts_request)

Получить список контактных лиц склада, удовлетворяющих условиям

Поиск объектов типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_contacts_request = new \Ensi\BuClient\Dto\SearchStoreContactsRequest(); // \Ensi\BuClient\Dto\SearchStoreContactsRequest | 

try {
    $result = $apiInstance->searchStoreContacts($search_store_contacts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->searchStoreContacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_contacts_request** | [**\Ensi\BuClient\Dto\SearchStoreContactsRequest**](../Model/SearchStoreContactsRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoreContactsResponse**](../Model/SearchStoreContactsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

